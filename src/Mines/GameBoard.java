package Mines;

import java.util.Observable;

public class GameBoard extends Observable {

    //protected int
    public int[][] fieldList;
    public BombField bombField;
    protected int fieldSize;

    public GameBoard(int fieldSize, int bombs) {

        bombField = new BombField(fieldSize, bombs);
        //System.out.println(bombField);
        this.fieldSize = fieldSize;

        this.fieldList = new int[fieldSize][fieldSize];

        for (int i = 0; i < fieldSize; i++) {

            for (int j = 0; j < fieldSize; j++) {

                fieldList[i][j] = 11;
            }
        }
    }

    public void openSquare(int i, int j){

        if (fieldList[i][j] == 11 || fieldList[i][j] == 12) {
            fieldList[i][j] = bombField.bombList[i][j];

            safeGroundTracker(i, j);

            this.setChanged();
            this.notifyObservers();
        }
    }

    private void safeGroundTracker(int i , int j){
        if (fieldList[i][j] == 0) {

            for (int x = i-1; x <= i + 1; x++) {

                for (int y = j-1; y <= j + 1; y++) {

                    if (x >= 0 && y >= 0 && x < fieldSize && y < fieldSize && fieldList[x][y] == 11) {
                        fieldList[x][y] = bombField.bombList[x][y];
                        safeGroundTracker(x, y);
                    }
                }
            }
        }
    }

    public void flagSquare(int i, int j){

        if (fieldList[i][j] == 11) {
            fieldList[i][j] = 12;
            this.setChanged();
            this.notifyObservers();

        } else if (fieldList[i][j] == 12) {
            fieldList[i][j] = 11;
            this.setChanged();
            this.notifyObservers();
        }
    }
}