package Mines;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Observable;
import java.util.Observer;

public class View extends Application implements Observer {

    private int fieldSize = 20;
    private int bombs = 45;
    private int correctCounter = 0;
    private GridPane gameField;
    private GameBoard gameBoard;
    private BorderPane border;
    private Boolean continueGame = true;
    private Boolean win = false;
    private Boolean lose = false;
    private int uncoveredCounter;
    private Label label;
    private HBox hBox;



    @Override
    public void start(Stage primaryStage) throws Exception {


        gameBoard = new GameBoard(fieldSize, bombs);
        gameBoard.addObserver(this);
        generateGameField();

        label = new Label();
        label.setText("good luck");
        label.setFont(Font.font(25));

        hBox = new HBox(label);
        hBox.setAlignment(Pos.CENTER);
        hBox.setStyle("-fx-background-color: #99ddff;");
        hBox.setPrefHeight(40);

        border = new BorderPane();
        border.setCenter(gameField);
        border.setTop(hBox);


        // GUI scene
        Scene scene = new Scene(border, 40 * fieldSize, 40 + 40 * fieldSize);
        primaryStage.setTitle("DeinSweeper");
        primaryStage.setScene(scene);
        primaryStage.show();

    }


    public GridPane generateGameField(){

        uncoveredCounter = 0;

        gameField = new GridPane();

        for (int i = 0; i < fieldSize; i++) {

            for (int j = 0; j < fieldSize; j++) {

                gameField.add(generateButton(i, j), i, j);
            }
        }

        return gameField;
    }


    public Button generateButton (int column, int row){

        Button temp = new Button();
        if (gameBoard.fieldList[column][row] == 11) {
            temp.setText(" ");
        } else if (gameBoard.fieldList[column][row] == 12) {
            temp.setText("F");
            temp.setStyle("-fx-background-color: #ffff00;");
        } else if (gameBoard.fieldList[column][row] == 0) {
            temp.setText("_");
            temp.setStyle("-fx-background-color: #99ddff;");
            uncoveredCounter++;
        // hit a bomb --> player loses
        } else if (gameBoard.fieldList[column][row] == 10) {
            temp.setText("B");
            temp.setStyle("-fx-background-color: #ff471a;");
            continueGame = false;
            lose = true;
        } else {
            temp.setText(String.valueOf(gameBoard.fieldList[column][row]));
            temp.setStyle("-fx-background-color: #99ddff;");
            uncoveredCounter++;
        }
        temp.setUserData(new Coordinates(column, row));
        temp.setPrefSize(40, 40);
        temp.setFont(Font.font(15));

        temp.setOnAction(event -> {
        });
        temp.setOnMousePressed(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                temp.arm();
            }
            if (event.getButton() == MouseButton.PRIMARY) {
                temp.arm();
            }
        });
        temp.setOnMouseReleased(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {

                if (continueGame) {
                    temp.disarm();
                    if (temp.getText().equals(" ") || temp.getText().equals("F")) {
                        gameBoard.flagSquare(column, row);
                    }
                }
            }
            if (event.getButton() == MouseButton.PRIMARY) {

                if (continueGame) {
                    temp.disarm();
                    gameBoard.openSquare(column, row);
                }
            }
        });

        // found all safe spots --> player wins
        if (uncoveredCounter == fieldSize * fieldSize - bombs) {
            continueGame = false;
            win = true;
        }

        return temp;
    }


    @Override
    public void update(Observable o, Object arg) {

        generateGameField();
        border.setCenter(gameField);

        if (!continueGame) {
            if (win) {
                label.setText("You won!!");
                hBox.setStyle("-fx-background-color: #33ff33;");
            } else {
                label.setText("Better luck next time");
                hBox.setStyle("-fx-background-color: #ff3300;");
            }
        }
    }
}
