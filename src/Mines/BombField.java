package Mines;

public class BombField {

    public int[][] bombList;


    public BombField(int fieldSize, int bombs) {

        generateNewField(fieldSize, bombs);
    }


    private void generateNewField(int fieldSize, int bombs) {

        int counter = 0;
        this.bombList = new int[fieldSize][fieldSize];

        // bury mines
        while (counter < bombs) {
            int column = getRandomInt(0, fieldSize-1);
            int row = getRandomInt(0, fieldSize-1);

            if (this.bombList[row][column] == 0) {
                this.bombList[row][column] = 10;

                counter++;
            }
        }

        // calc field values
        for (int i = 0; i < fieldSize; i++) {

            for (int j = 0; j < fieldSize; j++) {

                if (this.bombList[i][j] != 10) {
                    this.bombList[i][j] = getFieldValue(i, j, fieldSize);
                }
            }
        }
    }


    public int getFieldValue(int i, int j, int fieldSize){
        int value = 0;

        for (int x = i-1; x <= i + 1; x++) {

            for (int y = j-1; y <= j + 1; y++) {

                if (x >= 0 && y >= 0 && x < fieldSize && y < fieldSize && this.bombList[x][y] == 10) {
                    value += 1;
                }
            }
        }

        return value;
    }

    public int getRandomInt(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }


    @Override
    public String toString() {

        for (int i = 0; i < this.bombList.length; i++) {

            for (int j = 0; j < this.bombList.length; j++) {

                System.out.print(this.bombList[j][i] + ", ");
            }
            System.out.println();
        }
        return "check console";
    }
}
