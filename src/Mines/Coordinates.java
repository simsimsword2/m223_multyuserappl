package Mines;

public class Coordinates {

    public int column;
    public int row;

    public Coordinates(int column, int row) {
        this.column = column;
        this.row = row;
    }
}
