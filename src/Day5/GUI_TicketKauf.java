package Day5;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class GUI_TicketKauf extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        // title -------------------------------------------------
        HBox titlebox = new HBox();
        Text title = new Text("Ticket kaufen");
        title.setFont(Font.font(20));

        titlebox.getChildren().add(title);
        titlebox.setAlignment(Pos.CENTER);
        titlebox.setPadding(new Insets(5 ,5, 10, 5));

        // grid ------------------------------------------------
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(0, 25, 0, 25));
        grid.setHgap(25);
        grid.setVgap(5);

        // grid with 50 50 width split
        ColumnConstraints colcon = new ColumnConstraints();
        colcon.setPercentWidth(50);
        grid.getColumnConstraints().add(colcon);
        grid.getColumnConstraints().add(colcon);

        // labels
        Text event = new Text("Event");
        title.setFont(Font.font(15));
        Text name = new Text("Name");
        title.setFont(Font.font(15));
        Text vname = new Text("Vorname");
        title.setFont(Font.font(15));
        Text bemerkung = new Text("Bemerkung");
        title.setFont(Font.font(15));

        TextField nameField = new TextField();
        TextField vNameField = new TextField();

        TextArea bemerkungArea = new TextArea();
        bemerkungArea.setPrefHeight(100);

        // allignment
        GridPane.setValignment(event, VPos.TOP);
        GridPane.setValignment(bemerkung, VPos.TOP);

        // List view
        ListView listView = new ListView();
        ObservableList<String> eventList = FXCollections.observableArrayList ("A", "B", "C", "D");
        listView.setItems(eventList);

        listView.setPrefHeight(95);
        listView.setPrefWidth(150);

        // populate the grid
        grid.add(event, 0, 0);
        grid.add(listView, 1, 0);
        grid.add(name, 0,1);
        grid.add(nameField, 1, 1);
        grid.add(vname, 0, 2);
        grid.add(vNameField, 1, 2);
        grid.add(bemerkung, 0, 3);
        grid.add(bemerkungArea, 1, 3);


        // bottom --------------------------------------------------
        HBox hbox = new HBox();
        Button btn1 = new Button();
        Button btn2 = new Button();

        btn1.setText("OK");
        btn2.setText("Abbrechen");

        hbox.getChildren().add(btn1);
        hbox.getChildren().add(btn2);
        hbox.setAlignment(Pos.CENTER_RIGHT);
        hbox.setPadding(new Insets(10, 10, 10, 0));


        // border pane
        BorderPane border = new BorderPane();
        border.setPadding(new Insets(5, 10, 5, 10));
        border.setTop(titlebox);
        border.setCenter(grid);
        border.setBottom(hbox);


        // scene of GUI
        Scene scene = new Scene(border, 500, 350);
        primaryStage.setTitle("Ticket kaufen");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
