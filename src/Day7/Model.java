package Day7;


import java.util.Observable;
import java.util.Observer;

public class Model extends Observable {
    private int counter = 0;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
        this.setChanged();
        this.notifyObservers();
    }
}
