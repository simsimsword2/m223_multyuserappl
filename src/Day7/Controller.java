package Day7;

public class Controller {
    Model model = null;

    public Controller(Model model) {
        this.model = model;
    }

    public void increment(){
        this.model.setCounter(this.model.getCounter() + 1);
    }
}
