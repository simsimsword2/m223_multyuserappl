package Day7;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Observable;
import java.util.Observer;

public class View extends Application implements Observer {

    Model model = new Model();
    Controller controller = new Controller(model);
    Label label = null;

    @Override
    public void update(Observable o, Object arg) {
        label.setText(String.valueOf(model.getCounter()));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        model.addObserver(this);

        BorderPane border = new BorderPane();
        label = new Label(String.valueOf(model.getCounter()));
        label.setFont(Font.font(70));
        border.setCenter(label);

        Button button = new Button("Click me :)");
        button.setPrefWidth(200);
        button.setFont(Font.font(20));
        button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                controller.increment();
            }
        });
        border.setBottom(button);

        // GUI scene
        Scene scene = new Scene(border, 200, 200);
        primaryStage.setTitle("To infinity and beyond!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
