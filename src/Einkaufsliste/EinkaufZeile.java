package Einkaufsliste;

import java.time.LocalDate;

public class EinkaufZeile {

    private int articleNumber;
    private String article;
    private Double prize;
    private int amount;
    private String description;
    private LocalDate date;

    public int getArticleNumber() {
        return articleNumber;
    }

    public void setArticleNumber(int articleNumber) {
        this.articleNumber = articleNumber;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Double getPrize() {
        return prize;
    }

    public void setPrize(Double prize) {
        this.prize = prize;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public EinkaufZeile(int articleNumber, String article, Double prize, int amount, String description, LocalDate date) {
        this.articleNumber = articleNumber;
        this.article = article;
        this.prize = prize;
        this.amount = amount;
        this.description = description;
        this.date = date;
    }

    @Override
    public boolean equals(Object obj) {
        EinkaufZeile temp = (EinkaufZeile) obj;

        if (this.articleNumber == temp.getArticleNumber() &&
            this.article.equals(temp.getArticle()) &&
            this.prize.equals(temp.getPrize()) &&
            this.amount == temp.getAmount() &&
            this.description.equals(temp.getDescription()) &&
            this.date.equals(temp.getDate())
        ) {
            return true;
        } else {
            return false;
        }
    }
}
