package Einkaufsliste;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.sql.SQLException;

public class Model {

    ObservableList<EinkaufZeile> tableList = FXCollections.observableArrayList();

    public Model() {
        setTableList();
    }

    public ObservableList<EinkaufZeile> getTableList() {
        return tableList;
    }

    public void setTableList() {

        try {
            DAO_Einkauf dao = new DAO_Einkauf();
            this.tableList.setAll(dao.getAll());
            Connector.getConnection().close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
