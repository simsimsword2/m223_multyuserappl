package Einkaufsliste;

import Day2.DB_Person;
import com.sun.corba.se.spi.servicecontext.UEInfoServiceContext;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;


public class DAO_Einkauf {

    protected int insertRow(EinkaufZeile einkaufZeile){
        int recNumber = 0;

        try {
            Connection con = Connector.getConnection();

            PreparedStatement pstat = con.prepareStatement("insert into EList(Artikel, Preis, Menge, Beschreibung, Datum) values (?, ?, ?, ?, ?)");

            pstat.setString(1, einkaufZeile.getArticle());
            pstat.setDouble(2, einkaufZeile.getPrize());
            pstat.setInt(3, einkaufZeile.getAmount());
            pstat.setString(4, einkaufZeile.getDescription());
            pstat.setDate(5, Date.valueOf(einkaufZeile.getDate()));

            recNumber = pstat.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return recNumber;
    }

    protected ArrayList<EinkaufZeile> getAll(){
        ArrayList<EinkaufZeile> output = new ArrayList<EinkaufZeile>();
        ResultSet result = null;

        try {
            Connection con = Connector.getConnection();

            PreparedStatement pstat = con.prepareStatement("select Artikelnummer, Artikel, Preis, Menge, Beschreibung, Datum from EList");

            result = pstat.executeQuery();

            // iterate result
            while (result.next())
            {
                EinkaufZeile temp = new EinkaufZeile(result.getInt("Artikelnummer"),
                        result.getString("Artikel"),
                        result.getDouble("Preis"),
                        result.getInt("Menge"),
                        result.getString("Beschreibung"),
                        result.getDate("Datum").toLocalDate()
                );
                output.add(temp);
            }
            pstat.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return output;
    }

    protected Boolean exists(String article, LocalDate date){
        ResultSet recNumber = null;
        Boolean output = false;

        try {
            Connection con = Connector.getConnection();

            PreparedStatement pstat = con.prepareStatement("select Artikelnummer, Artikel, Preis, Menge, Beschreibung, Datum from EList where Artikel = ? and Datum = ?");

            pstat.setString(1, article);
            pstat.setDate(2, Date.valueOf(date));

            recNumber = pstat.executeQuery();

            if (recNumber.next()) {
                output = true;
            } else {
                output = false;
            }

            pstat.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return output;
    }
}
