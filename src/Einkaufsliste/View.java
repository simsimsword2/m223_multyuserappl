package Einkaufsliste;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.time.LocalDate;

public class View extends Application {

    Model model = new Model();
    Transaktion transaktion = new Transaktion(model);

    @Override
    public void start(Stage primaryStage) throws Exception {

        // border pane
        BorderPane border = new BorderPane();

        // title
        HBox titleBox = new HBox(newText("Meine Einkaufsliste", 20));
        titleBox.setAlignment(Pos.CENTER);
        border.setTop(titleBox);

        // table
        TableView tableView = new TableView<>();
        tableView.setEditable(true);

        TableColumn tcID = newTableColumn("NR", 80);
        TableColumn tcArticle = newTableColumn("Artikel", 90);
        TableColumn tcPrize = newTableColumn("Preis", 50);
        TableColumn tcAmount = newTableColumn("Menge", 50);
        TableColumn tcDescription = newTableColumn("Beschreibung", 150);
        TableColumn tcDate = newTableColumn("Datum", 80);
        tableView.getColumns().addAll(tcID,
                                      tcArticle,
                                      tcPrize,
                                      tcAmount,
                                      tcDescription,
                                      tcDate
                                     );

       tcID.setCellValueFactory(
                new PropertyValueFactory<EinkaufZeile,String>("articleNumber")
        );
       tcArticle.setCellValueFactory(
                new PropertyValueFactory<EinkaufZeile,String>("article")
        );
       tcPrize.setCellValueFactory(
                new PropertyValueFactory<EinkaufZeile,String>("prize")
        );
       tcAmount.setCellValueFactory(
                new PropertyValueFactory<EinkaufZeile,String>("amount")
        );
       tcDescription.setCellValueFactory(
                new PropertyValueFactory<EinkaufZeile,String>("description")
        );
       tcDate.setCellValueFactory(
                new PropertyValueFactory<EinkaufZeile,String>("date")
        );

        tableView.setItems(model.getTableList());


        // center
        border.setCenter(tableView);

        // bottom
        TextField articleField = new TextField("mein Artikel");

        Spinner<Double> prizeSpinner = new Spinner(0.0, 999.99, 999.99, 0.1);
        prizeSpinner.setPrefWidth(100);
        prizeSpinner.setEditable(true);

        Spinner<Integer> amountSpinner = new Spinner(0, 99, 99, 1);
        amountSpinner.setPrefWidth(80);
        amountSpinner.setEditable(true);

        TextField descriptionField = new TextField("*Kommentar");

        DatePicker datePicker = new DatePicker(LocalDate.now());
        datePicker.setPrefWidth(150);
        datePicker.setShowWeekNumbers(true);

        HBox hBox = new HBox();
        hBox.getChildren().addAll(articleField, prizeSpinner, amountSpinner, descriptionField, datePicker);


        Button insertButton = new Button("Einfügen");
        insertButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                EinkaufZeile temp = new EinkaufZeile(0, articleField.getText(), prizeSpinner.getValue(), amountSpinner.getValue(), descriptionField.getText(), datePicker.getValue());
                transaktion.tryInsert(temp);
            }
        });


        HBox hBox2 = new HBox();
        hBox2.getChildren().add(insertButton);
        hBox2.setAlignment(Pos.CENTER_RIGHT);


        VBox bottomVBox = new VBox();
        bottomVBox.getChildren().addAll(hBox, hBox2);

        border.setBottom(bottomVBox);


        // GUI scene
        Scene scene = new Scene(border, 550, 500);
        primaryStage.setTitle("Einkaufen leicht gemacht");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    protected Node newTextField(String myText, int font) {
        TextField temp = new TextField(myText);
        temp.setFont(Font.font(font));
        temp.setText(myText);
        return temp;
    }

    protected Node newText(String myText, int font) {
        Text temp = new Text(myText);
        temp.setFont(Font.font(font));
        temp.setText(myText);
        return temp;
    }

    protected TableColumn newTableColumn(String myText, int width) {
        TableColumn temp = new TableColumn(myText);
        temp.setText(myText);
        temp.setPrefWidth(width);
        return temp;
    }
}
