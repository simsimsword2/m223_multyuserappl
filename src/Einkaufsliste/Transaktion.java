package Einkaufsliste;

import java.sql.Connection;
import java.sql.SQLException;

public class Transaktion {

    Model model = null;

    public Transaktion(Model model) {
        this.model = model;
    }

    public Transaktion() {
    }

    protected boolean tryInsert(EinkaufZeile einkaufZeile){

        Boolean didInsert  = false;

        DAO_Einkauf dao = new DAO_Einkauf();

        try {
            if (!dao.exists(einkaufZeile.getArticle(), einkaufZeile.getDate())) {
                Connection con = Connector.getConnection();

                if (dao.insertRow(einkaufZeile) > 0) {
                    con.commit();
                    con.close();

                    model.setTableList();
                    didInsert = true;
                }
            } else {
                System.out.println("duplicate");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return didInsert;
    }
}
