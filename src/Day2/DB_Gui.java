package Day2;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.sql.SQLException;

public class DB_Gui extends Application {
    private final Double ticketpreis = 16.95;

    public Double getTicketpreis() {
        return ticketpreis;
    }


    @Override
    public void start(Stage primaryStage) {
        // container
        Label label1 = new Label("Name");
        TextField textField1 = new TextField();

        Label label2 = new Label("Vorname");
        TextField textField2 = new TextField();

        Label label3 = new Label("Geburtsdatum");
        DatePicker datePicker1 = new DatePicker();

        Label label4 = new Label("Über 18?");
        CheckBox checkBox1 = new CheckBox();

        Label label5 = new Label("Anzahl Karten");
        Spinner<Integer> spinner1 = new Spinner<Integer>(1, 50, 1);
        spinner1.setEditable(false);

        Button btn1 = new Button();
        btn1.setText("senden");
        btn1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                DB_Person tempPerson = new DB_Person(
                        textField1.getText(),
                        textField2.getText(),
                        datePicker1.getValue(),
                        checkBox1.isSelected(),
                        spinner1.getValue(),
                        spinner1.getValue() * getTicketpreis()
                );
                //System.out.println(tempPerson);

                DB_PersonDAO con = new DB_PersonDAO();
                try {
                    con.insertPerson(tempPerson);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });



        GridPane grid1 = new GridPane();
        ColumnConstraints col = new ColumnConstraints();
        col.setPrefWidth(150);

        grid1.getColumnConstraints().add(col);

        grid1.add(label1, 0, 0);
        grid1.add(textField1, 1, 0);
        grid1.add(label2, 0, 1);
        grid1.add(textField2, 1, 1);
        grid1.add(label3, 0, 2);
        grid1.add(datePicker1, 1, 2);
        grid1.add(label4, 0, 3);
        grid1.add(checkBox1, 1, 3);
        grid1.add(label5, 0, 4);
        grid1.add(spinner1, 1, 4);
        grid1.add(btn1, 1, 5);


        // Border
        BorderPane border = new BorderPane();
        border.setCenter(grid1);

        // Final scene
        Scene scene = new Scene(border, 270, 180);

        primaryStage.setTitle("Calc");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
    public static void main(String[] args) {
        launch(args);
    }
}
