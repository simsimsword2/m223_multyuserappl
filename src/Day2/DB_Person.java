package Day2;

import java.time.LocalDate;

public class DB_Person {
    // variables
    private String name;
    private String vorname;
    private LocalDate geburtsdatum;
    private Boolean volljaehrig;
    private int anzahl;
    private double preis;


    // constructor
    public DB_Person(String name, String vorname, LocalDate geburtsdatum, Boolean volljaehrig, int anzahl, double preis) {
        this.name = name;
        this.vorname = vorname;
        this.geburtsdatum = geburtsdatum;
        this.volljaehrig = volljaehrig;
        this.anzahl = anzahl;
        this.preis = preis;
    }


    // getter and setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(LocalDate geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public Boolean getVolljaehrig() {
        return volljaehrig;
    }

    public void setVolljaehrig(Boolean volljaehrig) {
        this.volljaehrig = volljaehrig;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    // to string

    @Override
    public String toString() {
        return "DB_Person{" +
                "name='" + name + '\'' +
                ", vorname='" + vorname + '\'' +
                ", geburtsdatum=" + geburtsdatum +
                ", volljaehrig=" + volljaehrig +
                ", anzahl=" + anzahl +
                ", preis=" + preis +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        Boolean isEqual = false;

        DB_Person other = (DB_Person) obj;

        if (
                this.name.equals(other.getName()) &&
                this.vorname.equals(other.getVorname()) &&
                this.geburtsdatum.equals(other.getGeburtsdatum()) &&
                this.volljaehrig.equals(other.getVolljaehrig()) &&
                this.anzahl == other.getAnzahl() &&
                this.preis == other.getPreis()
        ) {
            isEqual = true;
        }

        return isEqual;
    }
}
