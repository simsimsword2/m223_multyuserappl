package Day2;

import javax.xml.transform.Result;
import java.security.Permission;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class DB_PersonDAO {
    private String userName = "root";
    private String password = "";
    private String dbms = "mysql";
    private String serverName = "localhost";
    private String portNumber = "3306";
    private String dbName = "m223";


    public Connection getConnection() throws SQLException {

        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);

        if (this.dbms.equals("mysql")) {
            conn = DriverManager.getConnection("jdbc:" + this.dbms + "://" + this.serverName + ":" + this.portNumber + "/" + dbName, connectionProps);
        }
        //System.out.println("Open: " + !conn.isClosed());
        return conn;
    }

    public int insertPerson(DB_Person tempPerson) throws SQLException {

        Connection con = null;
        int number = 0;


        try {

            con = getConnection();

            // check if this entry already exists on the DB
            if (personExists(tempPerson) < 0) {

                // insert
                PreparedStatement pstat = con.prepareStatement("insert into person (lastName, firstName, birthdate, legalAge, numTickets, prize) values(?, ?, ?, ?, ?, ?)");

                pstat.setString(1, tempPerson.getName());
                pstat.setString(2, tempPerson.getVorname());
                pstat.setDate(3, Date.valueOf(tempPerson.getGeburtsdatum()));
                pstat.setBoolean(4, tempPerson.getVolljaehrig());
                pstat.setInt(5, tempPerson.getAnzahl());
                pstat.setDouble(6, tempPerson.getPreis());

                number = pstat.executeUpdate();
                pstat.close();
            } else {
                System.out.println("duplicate entry");
            }

        } catch(SQLException e){
            System.out.println("Insert failed: " + e);
        }

        con.close();
        return number;
    }


    public int updateNumberOfTickets(int personId, int numberOfTickets, Connection tempCon) {

        int number = 0;
        int originalNumberOfTickets = -1;

        try {

            // select

            PreparedStatement select = tempCon.prepareStatement("select numTickets from person where personID = ?");
            select.setInt(1, personId);

            ResultSet selected = select.executeQuery();

            if (selected.next()) {
                originalNumberOfTickets = selected.getInt("numTickets");
            }


            // update
            //PreparedStatement update = tempCon.prepareStatement("update person set numTickets = ? where personID = ?");
            PreparedStatement update = tempCon.prepareStatement("update person set numTickets = ? where personID = ? and numTickets = ?");


            update.setInt(1, numberOfTickets);
            update.setInt(2, personId);
            update.setInt(3, originalNumberOfTickets);

            number = update.executeUpdate();
            update.close();
            select.close();


        } catch(SQLException e){
            System.out.println("update failed: " + e);
        }

        return number;
    }


    public ArrayList<DB_Person> viewAll() throws SQLException {

        Connection con = null;
        ResultSet result;
        ArrayList<DB_Person> output = new ArrayList<DB_Person>();

        try {

            con = getConnection();

            PreparedStatement pstat = con.prepareStatement("select personID, lastName, firstName, birthdate, legalAge, numTickets, prize from person");

            result = pstat.executeQuery();

            // iterate result
            while (result.next())
            {
                DB_Person tempPers = new DB_Person(result.getString("lastName"),
                                                   result.getString("firstName"),
                                                   result.getDate("birthdate").toLocalDate(),
                                                   result.getBoolean("legalAge"),
                                                   result.getInt("numTickets"),
                                                   result.getDouble("prize")
                                                  );
                output.add(tempPers);
            }
            pstat.close();

        } catch (SQLException e) {
            System.out.println("Select failed: " + e);
        }

        con.close();
        return output;
    }


    public int personExists(DB_Person tempPerson) throws SQLException {

        Connection con = null;
        ResultSet resultSet = null;
        int result = -1;
        ArrayList<DB_Person> output = new ArrayList<DB_Person>();

        try {

            con = getConnection();

            PreparedStatement pstat = con.prepareStatement("select personID from person where lastName = ? and firstName = ? and birthdate = ? and legalAge = ? and numTickets = ? and prize = ?");

            pstat.setString(1, tempPerson.getName());
            pstat.setString(2, tempPerson.getVorname());
            pstat.setDate(3, Date.valueOf(tempPerson.getGeburtsdatum()));
            pstat.setBoolean(4, tempPerson.getVolljaehrig());
            pstat.setInt(5, tempPerson.getAnzahl());
            pstat.setDouble(6, tempPerson.getPreis());

            resultSet = pstat.executeQuery();

            // if an entry was found
            if (resultSet.next()) {
                result = resultSet.getInt("personID");
            }

            pstat.close();

        } catch (SQLException e) {
            System.out.println("Select failed: " + e);
        }

        con.close();
        return result;
    }
}