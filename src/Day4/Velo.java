package Day4;

public class Velo {
    private int numGears;
    private String model;

    // A bike has one owner     Velo -->[1] Person
    private Person owner;


    public Velo(int numGears, String model, Person owner) {
        this.numGears = numGears;
        this.model = model;
        this.owner = owner;

        owner.getBikes().add(this);
    }

    public int getNumGears() {
        return numGears;
    }

    public void setNumGears(int numGears) {
        this.numGears = numGears;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Velo{" +
                "numGears=" + numGears +
                ", model='" + model + '\'' +
                ", owner=" + owner.getName() +
                '}';
    }
}
