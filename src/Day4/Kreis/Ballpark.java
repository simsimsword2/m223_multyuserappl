package Day4.Kreis;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;


public class Ballpark extends Application {

    Canvas can = null;
    ArrayList<Kreis> list = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws Exception {

        can = new Canvas();
        BorderPane border = new BorderPane();
        can.widthProperty().bind(border.widthProperty());
        can.heightProperty().bind(border.heightProperty());


        border.setCenter(can);

        Scene scene = new Scene(border, 400, 300);
        primaryStage.setTitle("Ball World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        draw();

    }

    public void draw() {
        GraphicsContext gc = can.getGraphicsContext2D();
        gc.setFill(Color.YELLOW);
        gc.fillRect(0, 0, can.getWidth(), can.getHeight());
        gc.fillOval(0, 0, 100, 100);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
