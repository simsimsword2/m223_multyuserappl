package Day4.Kreis;

import javafx.scene.paint.Color;

public class Kreis {
    private double radius;
    private double xPos;
    private double yPos;
    private Color color;


    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getxPos() {
        return xPos;
    }

    public void setxPos(double xPos) {
        this.xPos = xPos;
    }

    public double getyPos() {
        return yPos;
    }

    public void setyPos(double yPos) {
        this.yPos = yPos;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Kreis(double radius, double xPos, double yPos, Color color) {
        this.radius = radius;
        this.xPos = xPos;
        this.yPos = yPos;
        this.color = color;
    }


    public double area() {
        return Math.pow(this.radius, 2) * Math.PI;
    }

    public double circumference() {
        return this.radius * 2 * Math.PI;
    }

}
