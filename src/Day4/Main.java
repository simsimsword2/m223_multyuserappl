package Day4;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Person hans = new Person("Hans", 40);
        Person heidi = new Person("Heidi", 35);

        Velo v1 = new Velo(24, "Sprinter", hans);
        Velo v2 = new Velo(3, "Cruiser", hans);
        Velo v3 = new Velo(6, "Oma", heidi);

        System.out.println(hans);

/* - option 2 -
        hans.getBikes().add(v1);
        hans.getBikes().add(v2);

        heidi.getBikes().add(v3);
*/

/* - option 3 -
        ArrayList<Velo> hansVelo = new ArrayList<>();
        ArrayList<Velo> heidiVelo = new ArrayList<>();

        hansVelo.add(v1);
        hansVelo.add(v2);
        heidiVelo.add(v3);

        hans.setBikes(hansVelo);
        for (Velo v : hansVelo) {
            v.setOwner(hans);
        }

        heidi.setBikes(heidiVelo);
        for (Velo v : heidiVelo) {
            v.setOwner(heidi);
        }
*/
    }
}
