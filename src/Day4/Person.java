package Day4;

import java.util.ArrayList;

public class Person {
    private String name;
    private int alter;

    // A person can have 0-n bikes      Person -->[0-n] Velo
    private ArrayList<Velo> bikes;


    public Person(String name, int alter) {
        this.name = name;
        this.alter = alter;
        this.bikes = new ArrayList<>();
    }

    public Person(String name, int alter, ArrayList<Velo> bikes) {
        this.name = name;
        this.alter = alter;
        this.bikes = bikes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAlter() {
        return alter;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }

    public ArrayList<Velo> getBikes() {
        return bikes;
    }

    public void setBikes(ArrayList<Velo> bikes) {
        this.bikes = bikes;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", alter=" + alter +
                ", bikes=" + bikes +
                '}';
    }
}
