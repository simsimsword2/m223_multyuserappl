package Day3;

import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class KontoDAO implements IKontoDAO {
    private String userName = "root";
    private String password = "";
    private String dbms = "mysql";
    private String serverName = "localhost";
    private String portNumber = "3306";
    private String dbName = "m223";

    @Override
    public double bezug(double tempBetrag, int tempKontoNr) {
        double result = -99.99;

        Connection con = verbindung();
        try {
            con.setAutoCommit(false);

            ArrayList<Double> resultList = lesen(tempKontoNr, con);

            if (resultList.get(0) >= tempBetrag) {

                int anzRec = schreiben(resultList.get(0) - tempBetrag, tempKontoNr, con);

                if (anzRec > 0) {
                    con.commit();
                    result = resultList.get(0) - tempBetrag;
                    System.out.println(result);
                } else {
                    con.rollback();
                }

            } else {
                System.out.println("Zu wenig Geld!!");
            }

            con.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }

    @Override
    public Connection verbindung() {
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);

        try {
            conn = DriverManager.getConnection("jdbc:" + this.dbms + "://" + this.serverName + ":" + this.portNumber + "/" + dbName, connectionProps);
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println(e);
        }

        return conn;
    }

    @Override
    public ArrayList<Double> lesen(int tempKontoNr, Connection tempCon) {

        ArrayList<Double> resultList = new ArrayList<Double>();

        try {

            PreparedStatement query = tempCon.prepareStatement("select betrag from konto where kontoNr = ?");
            query.setInt(1, tempKontoNr);

            ResultSet resultset = query.executeQuery();

            while (resultset.next()) {
                resultList.add(resultset.getDouble("betrag"));
            }

            query.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return resultList;
    }

    @Override
    public int schreiben(double neuerKontostand, int tempKontoNr, Connection tempCon) {

        int anzRec = 0;

        try {

            PreparedStatement query = tempCon.prepareStatement("update konto set betrag = ? where kontoNr = ?");
            query.setDouble(1, neuerKontostand);
            query.setInt(2, tempKontoNr);

            anzRec = query.executeUpdate();

            query.close();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return anzRec;
    }
}
