package Day3;

import java.sql.Connection;
import java.util.ArrayList;

public interface IKontoDAO {

    double bezug(double Betrag, int KontoNR);

    Connection verbindung();

    ArrayList<Double> lesen(int KontoNR, Connection connection);

    int schreiben(double Kontostand, int KontoNR, Connection connection);
}
