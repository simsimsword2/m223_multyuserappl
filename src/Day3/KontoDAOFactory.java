package Day3;

public class KontoDAOFactory {
    public static final double version = 1.5;

    public static IKontoDAO createKontoDAO() {

        if (version >= 2.0) {
            return new BesserKontoDAO();
        } else {
            return new KontoDAO();
        }
    }
}
