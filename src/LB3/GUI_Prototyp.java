package LB3;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Date;

public class GUI_Prototyp extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {

        // title -------------------------------------------------
        HBox titlebox = new HBox();
        Text title = new Text("Ticket kaufen");
        title.setFont(Font.font(20));

        titlebox.getChildren().add(title);
        titlebox.setAlignment(Pos.CENTER);
        titlebox.setPadding(new Insets(5 ,5, 10, 5));

        // grid ------------------------------------------------
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(0, 25, 0, 25));
        grid.setHgap(25);
        grid.setVgap(5);

        // grid with 50 50 width split
        ColumnConstraints colcon1 = new ColumnConstraints();
        ColumnConstraints colcon2 = new ColumnConstraints();
        colcon1.setPercentWidth(25);
        colcon2.setPercentWidth(75);

        grid.getColumnConstraints().add(colcon1);
        grid.getColumnConstraints().add(colcon2);

        // labels
        Text films = new Text("Films");
        films.setFont(Font.font(15));
        Text name = new Text("Name");
        name.setFont(Font.font(15));
        Text vname = new Text("Vorname");
        vname.setFont(Font.font(15));
        Text sitzReihe = new Text("Sitz Reihe");
        sitzReihe.setFont(Font.font(15));
        Text sitzPlatz = new Text("Sitz Platz");
        sitzPlatz.setFont(Font.font(15));
        Text date = new Text("Besuchsdatum");
        date.setFont(Font.font(15));
        Text member = new Text("Sind Sie Mitglied?");
        member.setFont(Font.font(15));
        Text guestNumber = new Text("Ihre Kundennummer");
        guestNumber.setFont(Font.font(15));
        Text noGuest = new Text("Noch kein Mitglied?");
        noGuest.setFont(Font.font(15));
        Text agb = new Text("Sie haben die AGBs gelesen und akzeptiert");
        agb.setFont(Font.font(15));
        Text star = new Text("Bewertung");
        star.setFont(Font.font(15));


        TextField nameField = new TextField();
        nameField.setPrefWidth(100);
        TextField vNameField = new TextField();
        TextField guestNumberField = new TextField();

        DatePicker datePicker = new DatePicker();

        Spinner<Integer> spinnerReihe = new Spinner(1, 25, 1, 1);
        Spinner<Integer> spinnerPlatz = new Spinner(1, 40, 1, 1);
        Spinner<Integer> spinnerStar = new Spinner(1, 5, 1, 1);

        CheckBox checkMember = new CheckBox();

        CheckBox checkAgb = new CheckBox();

        Hyperlink linkAgb = new Hyperlink("AGB Link");


        // allignment
        GridPane.setValignment(films, VPos.TOP);

        // List view
        TableView tableView = new TableView();
        ObservableList<String> eventList = FXCollections.observableArrayList ("A", "B", "C", "D");
        tableView.setItems(eventList);

        TableColumn tcFilmNr = new TableColumn("Filmnummer");
        TableColumn tcFilm = new TableColumn("Film");
        TableColumn tcTime = new TableColumn("Zeit");
        TableColumn tcRoom = new TableColumn("Saal");

        tableView.getColumns().addAll(tcFilmNr, tcFilm, tcTime, tcRoom);

        tableView.setPrefHeight(200);
        tableView.setPrefWidth(100);


        // populate the grid
        grid.add(date, 0, 0);
        grid.add(datePicker, 1, 0);
        grid.add(films, 0,1);
        grid.add(tableView, 1, 1);
        grid.add(sitzReihe, 0, 2);
        grid.add(spinnerReihe, 1, 2);
        grid.add(sitzPlatz, 0, 3);
        grid.add(spinnerPlatz, 1, 3);
        grid.add(member, 0, 4);
        grid.add(checkMember, 1, 4);
        grid.add(guestNumber, 0, 5);
        grid.add(guestNumberField, 1, 5);
        grid.add(noGuest, 0, 6);
        grid.add(name, 0, 7);
        grid.add(nameField, 1, 7);
        grid.add(vname, 0, 8);
        grid.add(vNameField, 1, 8);
        grid.add(agb, 0, 9);
        grid.add(checkAgb, 1, 9);
        grid.add(linkAgb, 1, 10);
        grid.add(star, 0, 11);
        grid.add(spinnerStar, 1, 11);



        // bottom --------------------------------------------------
        HBox hbox = new HBox();
        Label lbl1 = new Label("Kauf fehlgeschlagen, bitte füllen Sie alle nötigen Felder aus.");
        Button btn1 = new Button("Kaufen");
        Button btn2 = new Button("Abbrechen");

        hbox.getChildren().add(lbl1);
        hbox.getChildren().add(btn1);
        hbox.getChildren().add(btn2);
        hbox.setAlignment(Pos.CENTER_RIGHT);
        hbox.setPadding(new Insets(10, 10, 10, 0));


        // border pane
        BorderPane border = new BorderPane();
        border.setPadding(new Insets(5, 10, 5, 10));
        border.setTop(titlebox);
        border.setCenter(grid);
        border.setBottom(hbox);


        // scene of GUI
        Scene scene = new Scene(border, 1200, 500);
        primaryStage.setTitle("Ticket kaufen");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
