package LB3;

import java.util.ArrayList;

public interface ITicketDAO {

    int insert(TicketData data);

    int update(TicketData data);

    int delete(int ticketNumber);

    ArrayList<TicketData> select(TicketData data);
}
