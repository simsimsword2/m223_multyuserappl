package LB3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class TicketConnector {
    private static Connection connection = null;
    private static String userName = "root";
    private static String password = "";
    private static String dbms = "mysql";
    private static String serverName = "localhost";
    private static String portNumber = "3306";
    private static String dbName = "m223";


    public static Connection getConnection() throws SQLException {

        if (connection == null || connection.isClosed()) {
            Properties connectionProps = new Properties();
            connectionProps.put("user", userName);
            connectionProps.put("password", password);

            if (dbms.equals("mysql")) {
                connection = DriverManager.getConnection("jdbc:" + dbms + "://" + serverName + ":" + portNumber + "/" + dbName, connectionProps);
            }
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        }

        return connection;
    }
}
