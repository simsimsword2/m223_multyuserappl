package LB3;

import java.sql.*;
import java.util.ArrayList;

public class TicketDAO implements ITicketDAO {

    @Override
    public int insert(TicketData data) {

        int result = 0;

        try {
            Connection con = TicketConnector.getConnection();

            PreparedStatement pstat = con.prepareStatement("insert into Ticket(Ticketnummer, Filmvorfuehrnummer, Sitzreihe, Sitzplatz, Kundennummer, NameKunde, VornameKunde, Kaufdatum, Bewertung)" +
                                                               "values(?,?,?,?,?,?,?,?,?)");
            pstat.setInt(1, data.getTicketNumber());
            pstat.setInt(2, data.getFilmNumber());
            pstat.setInt(3, data.getSeatRow());
            pstat.setInt(4, data.getSeatNr());
            pstat.setInt(5, data.getClientNr());
            pstat.setString(6, data.getName());
            pstat.setString(7, data.getFirstName());
            pstat.setDate(8, Date.valueOf(data.getDate()));
            pstat.setInt(9, data.getRating());

            // optimistic insert
            result = pstat.executeUpdate();

            ArrayList<TicketData> checkList = select(data);

            // optimistic insert -> now we must check, if the seat in the same film on the same date is already occupied
            if (checkList.size() > 1) {
                con.rollback();
                result = 0;
                System.out.println("Seat is already occupied");
            } else {
                con.commit();
            }

            pstat.close();
            con.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    @Override
    public int update(TicketData data) {
        return 0;
    }

    @Override
    public int delete(int ticketNumber) {
        return 0;
    }

    // returns a list of Tickets with the given seatnumber, row and film
    @Override
    public ArrayList<TicketData> select(TicketData data) {
        ArrayList<TicketData> returnList = new ArrayList<>();
        ResultSet resultSet = null;

        try {
            Connection con = TicketConnector.getConnection();

            PreparedStatement pstat = con.prepareStatement("select Ticketnummer, Filmvorfuehrnummer, Sitzreihe, Sitzplatz, Kundennummer, NameKunde, VornameKunde, Kaufdatum, Bewertung from Ticket where Filmvorfuehrnummer = ? and Sitzreihe = ? and Sitzplatz = ? and Kaufdatum = ?");

            pstat.setInt(1, data.getFilmNumber());
            pstat.setInt(2, data.getSeatRow());
            pstat.setInt(3, data.getSeatNr());
            pstat.setDate(4, Date.valueOf(data.getDate()));

            resultSet = pstat.executeQuery();

            while (resultSet.next()) {

                TicketData temp = new TicketData(
                        resultSet.getInt("Ticketnummer"),
                        resultSet.getInt("Filmvorfuehrnummer"),
                        resultSet.getInt("Sitzreihe"),
                        resultSet.getInt("Sitzplatz"),
                        resultSet.getInt("Kundennummer"),
                        resultSet.getString("NameKunde"),
                        resultSet.getString("VornameKunde"),
                        resultSet.getDate("Kaufdatum").toLocalDate(),
                        resultSet.getInt("Bewertung")
                );

                returnList.add(temp);
            }

            pstat.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return returnList;
    }
}
