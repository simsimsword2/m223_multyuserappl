use m223;

drop table if exists Ticket;
create table Ticket(
	   Ticketnummer int primary key auto_increment,
       Filmvorfuehrnummer int,
       SitzReihe int,
       SitzPlatz int,
       Kundennummer int,
       NameKunde char(30),
       VornameKunde char(30),
       Kaufdatum date,
       Bewertung int
);

select Ticketnummer, Filmvorfuehrnummer, Sitzreihe, Sitzplatz, Kundennummer, NameKunde, VornameKunde, Kaufdatum, Bewertung from Ticket;