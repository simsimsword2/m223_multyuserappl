package LB3;

import java.time.LocalDate;

public class TicketData {
    private int ticketNumber;
    private int filmNumber;
    private int seatRow;
    private int seatNr;
    private int clientNr;
    private String name;
    private String firstName;
    private LocalDate date;
    private int rating;


    public int getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public int getFilmNumber() {
        return filmNumber;
    }

    public void setFilmNumber(int filmNumber) {
        this.filmNumber = filmNumber;
    }

    public int getSeatRow() {
        return seatRow;
    }

    public void setSeatRow(int seatRow) {
        this.seatRow = seatRow;
    }

    public int getSeatNr() {
        return seatNr;
    }

    public void setSeatNr(int seatNr) {
        this.seatNr = seatNr;
    }

    public int getClientNr() {
        return clientNr;
    }

    public void setClientNr(int clientNr) {
        this.clientNr = clientNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }


    public TicketData(int ticketNumber, int filmNumber, int seatRow, int seatNr, int clientNr, String name, String firstName, LocalDate date, int rating) {
        this.ticketNumber = ticketNumber;
        this.filmNumber = filmNumber;
        this.seatRow = seatRow;
        this.seatNr = seatNr;
        this.clientNr = clientNr;
        this.name = name;
        this.firstName = firstName;
        this.date = date;
        this.rating = rating;
    }
}
