package Day1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class Calculator extends Application {
    // values
    Boolean dec_flag = false;
    String function = "=";
    double tempValue = 0;

    @Override
    public void start(Stage primaryStage) {
        // container

        // top - result
        Text result = new Text();
        result.setText(String.valueOf("0.0"));

        HBox hbox1 = new HBox();
        hbox1.getChildren().add(result);
        BackgroundFill bf = new BackgroundFill(Color.LIGHTBLUE, new CornerRadii(1), null);
        hbox1.setPadding(new Insets(10, 10, 10, 0));
        hbox1.setAlignment(Pos.CENTER_RIGHT);
        hbox1.setBackground(new Background(bf));

        //----------------------------------------------------------------

        // Buttons
        Button btn1 = new Button();
        btn1.setText("1");
        btn1.setPrefSize(50, 50);
        btn1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(1, Double.valueOf(result.getText()))));}
        });

        Button btn2 = new Button();
        btn2.setText("2");
        btn2.setPrefSize(50, 50);
        btn2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(2, Double.valueOf(result.getText()))));}
        });

        Button btn3 = new Button();
        btn3.setText("3");
        btn3.setPrefSize(50, 50);
        btn3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(3, Double.valueOf(result.getText()))));}
        });

        Button btn4 = new Button();
        btn4.setText("4");
        btn4.setPrefSize(50, 50);
        btn4.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(4, Double.valueOf(result.getText()))));}
        });

        Button btn5 = new Button();
        btn5.setText("5");
        btn5.setPrefSize(50, 50);
        btn5.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(5, Double.valueOf(result.getText()))));}
        });

        Button btn6 = new Button();
        btn6.setText("6");
        btn6.setPrefSize(50, 50);
        btn6.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(6, Double.valueOf(result.getText()))));}
        });

        Button btn7 = new Button();
        btn7.setText("7");
        btn7.setPrefSize(50, 50);
        btn7.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(7, Double.valueOf(result.getText()))));}
        });

        Button btn8 = new Button();
        btn8.setText("8");
        btn8.setPrefSize(50, 50);
        btn8.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(8, Double.valueOf(result.getText()))));}
        });

        Button btn9 = new Button();
        btn9.setText("9");
        btn9.setPrefSize(50, 50);
        btn9.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(9, Double.valueOf(result.getText()))));}
        });

        Button btn10 = new Button();
        btn10.setText("0");
        btn10.setPrefSize(50, 50);
        btn10.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(enter_number(0, Double.valueOf(result.getText()))));}
        });

        Button btn11 = new Button();
        btn11.setText("+");
        btn11.setPrefSize(50, 50);
        btn11.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {tempValue = Double.valueOf(result.getText()); result.setText("0.0"); dec_flag = false; function = "+";}
        });
        Button btn12 = new Button();
        btn12.setText("-");
        btn12.setPrefSize(50, 50);
        btn12.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {tempValue = Double.valueOf(result.getText()); result.setText("0.0"); dec_flag = false; function = "-";}
        });

        Button btn13 = new Button();
        btn13.setText("*");
        btn13.setPrefSize(50, 50);
        btn13.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {tempValue = Double.valueOf(result.getText()); result.setText("0.0"); dec_flag = false; function = "*";}
        });

        Button btn14 = new Button();
        btn14.setText("/");
        btn14.setPrefSize(50, 50);
        btn14.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {tempValue = Double.valueOf(result.getText()); result.setText("0.0"); dec_flag = false; function = "/";}
        });

        Button btn15 = new Button();
        btn15.setText("C");
        btn15.setPrefSize(50, 50);
        btn15.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {dec_flag = true; tempValue = 0; result.setText("0.0");}
        });

        Button btn16 = new Button();
        btn16.setText("=");
        btn16.setPrefSize(50, 50);
        btn16.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {result.setText(String.valueOf(calc_value(Double.valueOf(result.getText()))));}
        });



        // center
        GridPane grid1 = new GridPane();
        ColumnConstraints col = new ColumnConstraints();
        col.setPercentWidth(25);

        //grid1.getColumnConstraints().add(col);
        //grid1.getColumnConstraints().add(col);
        //grid1.getColumnConstraints().add(col);
        //grid1.getColumnConstraints().add(col);

        // numbers
        grid1.add(btn1, 0, 0);
        grid1.add(btn2, 1, 0);
        grid1.add(btn3, 2, 0);
        grid1.add(btn4, 0, 1);
        grid1.add(btn5, 1, 1);
        grid1.add(btn6, 2, 1);
        grid1.add(btn7, 0, 2);
        grid1.add(btn8, 1, 2);
        grid1.add(btn9, 2, 2);
        grid1.add(btn10, 1, 3);
        // functions
        grid1.add(btn11, 3, 0);
        grid1.add(btn12, 3, 1);
        grid1.add(btn13, 3, 2);
        grid1.add(btn14, 3,3);
        // additional
        grid1.add(btn15, 0, 3);
        grid1.add(btn16, 2, 3);
        //----------------------------------------------------------------


        // Border
        BorderPane border = new BorderPane();
        border.setTop(hbox1);
        border.setCenter(grid1);

        // Final scene
        Scene scene = new Scene(border, 200, 220);

        primaryStage.setTitle("Calc");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
    public static void main(String[] args) {
        launch(args);
    }

    private double enter_number(int number, double oldNumber){

        return oldNumber * 10 + number;
    }

    private double calc_value(double userInput){
        switch (function) {
            case "+":
                System.out.println(tempValue + function + userInput);
                return tempValue + userInput;

            case "-":
                return tempValue - userInput;

            case "*":
                return tempValue * userInput;

            case "/":
                return tempValue / userInput;

            default:
                return userInput;
        }
    }
}
