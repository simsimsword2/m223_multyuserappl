package Day1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ManyButton extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Buttons
        Button btn1 = new Button();
        btn1.setText("B1");
        btn1.setPrefSize(100, 30);
        Button btn2 = new Button();
        btn2.setText("B2");
        btn2.setPrefSize(100, 30);
        Button btn3 = new Button();
        btn3.setText("B3");
        btn3.setPrefSize(100, 30);
        Button btn4 = new Button();
        btn4.setText("B4");
        btn4.setPrefSize(100, 30);
        Button btn5 = new Button();
        btn5.setText("B5");
        btn5.setPrefSize(100, 30);
        Button btn6 = new Button();
        btn6.setText("B6");
        btn6.setPrefSize(100, 30);
        Button btn7 = new Button();
        btn7.setText("B7");
        btn7.setPrefSize(30, 100);
        Button btn8 = new Button();
        btn8.setText("B8");
        btn8.setPrefSize(30, 30);
        Button btn9 = new Button();
        btn9.setText("B9");
        btn9.setPrefSize(30, 30);
        Button btn10 = new Button();
        btn10.setText("B10");
        btn10.setPrefSize(300, 30);
        Button btn11 = new Button();
        btn11.setText("B11");
        btn11.setPrefSize(150, 30);
        Button btn12 = new Button();
        btn12.setText("B12");
        btn12.setPrefSize(150, 30);


        // container

        // top1
        HBox hbox1 = new HBox();
        hbox1.setSpacing(1);
        hbox1.getChildren().add(btn1);
        hbox1.getChildren().add(btn2);
        hbox1.getChildren().add(btn3);

        // top2
        HBox hbox2 = new HBox();
        hbox2.setSpacing(1);
        hbox2.getChildren().add(btn4);
        hbox2.getChildren().add(btn5);
        hbox2.getChildren().add(btn6);

        // top: top1 + top2
        VBox vbox1 = new VBox();
        vbox1.getChildren().add(hbox1);
        vbox1.getChildren().add(hbox2);
        //----------------------------------------------------------------

        // bottom1
        HBox hbox3 = new HBox();
        hbox3.getChildren().add(btn11);
        hbox3.getChildren().add(btn12);

        // bottom: bottom1 + button 10
        VBox vbox2 = new VBox();
        vbox2.getChildren().add(hbox3);
        vbox2.getChildren().add(btn10);
        //----------------------------------------------------------------

        // center
        HBox hbox4 = new HBox();
        hbox4.getChildren().add(btn8);
        hbox4.getChildren().add(btn9);
        hbox4.setAlignment(Pos.TOP_CENTER);
        hbox4.prefHeight(150);
        //----------------------------------------------------------------

        // Border
        BorderPane border = new BorderPane();
        border.setTop(vbox1);
        border.setLeft(btn7);
        border.setCenter(hbox4);
        border.setBottom(vbox2);

        // Final scene
        Scene scene = new Scene(border, 300, 220);

        primaryStage.setTitle("Button World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
