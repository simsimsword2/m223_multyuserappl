package Day4.Kreis;

import javafx.scene.paint.Color;
import org.junit.Test;

import static org.junit.Assert.*;

public class KreisTest {
    Kreis k1 = new Kreis(3, 1, 2, Color.BLUE);

    @Test
    public void area() {
        double control = 9 * Math.PI;

        assertEquals(0, Double.compare(control, k1.area()));
    }

    @Test
    public void circumference() {
        double control = 6 * Math.PI;

        assertEquals(0, Double.compare(control, k1.circumference()));
    }
}