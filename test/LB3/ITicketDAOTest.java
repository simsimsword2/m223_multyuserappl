package LB3;

import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ITicketDAOTest {


    ITicketDAO dao = new TicketDAO();
    TicketData testData1 = new TicketData(1, 13, 25, 3, 123456, "", "", LocalDate.now(), 4);
    TicketData testData2 = new TicketData(2, 13, 25, 3, 345678, "", "", LocalDate.now(), 2);
    TicketData testData3 = new TicketData(3, 11, 11, 6, 000000, "", "", LocalDate.now(), 4);
    TicketData testData4 = new TicketData(1, 1, 25, 3, 123456, "", "", LocalDate.now(), 1);


    @Test
    public void insert() {
        // fist insert
        assertEquals(1, dao.insert(testData1));


        // fail because the seat is already occupied
        assertEquals(0, dao.insert(testData2));
    }

    @Test
    public void update() {
        // updated filmNumber and rating of the first entry
        assertEquals(1, dao.update(testData4));

        // fail because the id was not found
        assertEquals(0, dao.update(testData3));
    }

    @Test
    public void delete() {
        // deleted
        assertEquals(1, dao.delete(1));

        // fail, id not found
        assertEquals(0, dao.delete(15));
    }

    @Test
    public void select() {
        // This select returns a list of TicketDate where the seat row, seat number, film number and date are the same -> these seats might have been sold more than once and can not be committed.
        ArrayList testList = dao.select(testData1);
        assertEquals(1, testList.size());

        // Here the select return nothing so the seat has not yet been sold

        testList = dao.select(testData3);
        assertEquals(0, testList.size());

    }
}