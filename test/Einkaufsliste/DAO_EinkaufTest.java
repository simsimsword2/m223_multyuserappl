package Einkaufsliste;

import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class DAO_EinkaufTest {

    DAO_Einkauf dao = new DAO_Einkauf();
    EinkaufZeile einkaufZeile = new EinkaufZeile(1, "Spaghetti", 4.95, 1 , "500g bitte", LocalDate.of(2021, 11, 16));
    EinkaufZeile einkaufZeile2 = new EinkaufZeile(2, "Seife", 13.30, 2 , "Nicht die rote Seife!", LocalDate.of(2021, 11, 16));

    @Test
    public void insertRow() {
        assertEquals(1, dao.insertRow(einkaufZeile));

        assertEquals(1, dao.insertRow(einkaufZeile2));
    }

    @Test
    public void getAll() {
        ArrayList<EinkaufZeile> res = dao.getAll();

        if (res.size() > 1) {
            try {
                Connection con = Connector.getConnection();
                assertTrue(res.get(0).equals(einkaufZeile));
                assertTrue(res.get(1).equals(einkaufZeile2));
                con.commit();
                con.close();

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        } else {
            assertTrue(false);
        }
    }

    @Test
    public void exists() {
        assertTrue(dao.exists("Seife", LocalDate.of(2021, 11, 16)));
    }
}