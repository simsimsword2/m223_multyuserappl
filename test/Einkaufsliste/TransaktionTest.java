package Einkaufsliste;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class TransaktionTest {

    @Test
    public void tryInsert() {
        Transaktion tra = new Transaktion();
        EinkaufZeile einkaufZeile = new EinkaufZeile(1, "Spaghetti", 4.95, 1 , "500g bitte", LocalDate.of(2021, 11, 16));

        assertTrue(tra.tryInsert(einkaufZeile));
        assertFalse(tra.tryInsert(einkaufZeile));
    }
}