package Day2;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import static java.sql.Connection.TRANSACTION_NONE;
import static org.junit.Assert.*;


//                                                                                //
//// Before testing please execute the sql file, then run the whole test class. ////
//                                                                                //
public class DB_PersonDAOTest {

    DB_PersonDAO test = new DB_PersonDAO();
    DB_Person tempPerson1 = new DB_Person("test name", "test  first name", LocalDate.now(),  true, 2, 22.22);
    DB_Person tempPerson2 = new DB_Person("test name two", "test second first name", LocalDate.now(),  false, 4, 44.44);

    @org.junit.Test
    public void getConnection() throws SQLException {
        boolean success = false;
        try {
            Connection result = test.getConnection();
            if (result != null && !result.isClosed()) {
                success = true;
                result.close();
            }
        } catch (SQLException e) {
            System.out.println("Connection was not successful" + e);
        }
         assertTrue(success);
    }


    @Test
    public void insertPerson1() throws SQLException {

        assertEquals(1, test.insertPerson(tempPerson1));
    }


    @Test
    public void insertPerson2() throws SQLException {

        assertEquals(1, test.insertPerson(tempPerson2));
    }


    // duplicate entry is not allowed
    @Test
    public void insertPerson3() throws SQLException {

        assertEquals(0, test.insertPerson(tempPerson2));
    }


    @Test
    public void viewAll() throws SQLException {

        ArrayList<DB_Person> testList = new ArrayList<DB_Person>();
        testList.add(tempPerson1);
        testList.add(tempPerson2);

        ArrayList<DB_Person> resultList = test.viewAll();

        assertEquals(testList.get(0), resultList.get(0));
        assertEquals(testList.get(1), resultList.get(1));
    }


    @Test
    public void personExists() throws SQLException {

        assertEquals(1, test.personExists(tempPerson1));
        assertEquals(2, test.personExists(tempPerson2));
    }

    @Test
    public void updateNumberOfTickets() {
        try {

            Connection con = test.getConnection();
            con.setAutoCommit(false);
            //con.setTransactionIsolation(TRANSACTION_NONE);

            int numberOfChangedRec = test.updateNumberOfTickets(1, 10, con);

            con.commit();

            assertEquals(1, numberOfChangedRec);


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    public void updateNumberOfTickets2() {
        try {

            Connection con = test.getConnection();
            con.setAutoCommit(false);
            //con.setTransactionIsolation(0);

            int numberOfChangedRec = test.updateNumberOfTickets(1, 25, con);

            con.commit();

            assertEquals(0, numberOfChangedRec);


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}