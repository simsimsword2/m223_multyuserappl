package Day3;

import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class IKontoDAOTest {
    // factory returns the newest version of KontoDAO
    IKontoDAO dao1 = KontoDAOFactory.createKontoDAO();
    Connection con1 = null;

    int kontoNr1 = 12858277;
    double betrag1 = 100.10;
    int kontoNr2 = 12635277;
    double betrag2 = 20.10;

    @Test
    public void bezug() {
        assertEquals(0,  Double.compare(9900.40 , dao1.bezug(betrag1, kontoNr1)));
    }

    @Test
    public void verbindung() {
        con1 = dao1.verbindung();

        Boolean didWork = false;

        try {
            if (!con1.isClosed()) {
                didWork = true;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        assertTrue(didWork);
    }

    @Test
    public void lesen() {
        con1 = dao1.verbindung();

        ArrayList<Double> testList = dao1.lesen(kontoNr2, con1);

        assertEquals(0,  Double.compare(20000.40 , testList.get(0)));
    }

    @Test
    public void schreiben() {
        con1 = dao1.verbindung();

        assertEquals(1, dao1.schreiben(100.00 - betrag2, kontoNr2, con1));
    }
}