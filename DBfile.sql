-- DB
-- drop database if exists M223;

-- create database M223;
use M223;

-- Table for person
drop table if exists person;

create table person(
	personID int not null primary key auto_increment,
    lastName varchar(50) not null,
    firstName varchar(50) not null,
    birthdate date not null,
    legalAge bit not null,
    numTickets int not null,
    prize decimal(10, 2) not null
);

insert into person(lastName, firstName, birthdate, legalAge, numTickets, prize) values('Test', 'Max', '2020-12-25', false, 4, 11.30);
select personID, lastName, firstName, birthdate, legalAge, numTickets, prize from person;

create database e04;
use e04;
create table lostupdate(
	id int not null auto_increment,
    alpha int,
    constraint pk_ik primary key (id)
);

insert into lostupdate (alpha) values (69);

