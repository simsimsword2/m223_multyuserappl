-- DB
-- drop database if exists M223;

-- create database M223;
use M223;

-- Table for person
drop table if exists konto;

create table konto(
	kontoNr int not null primary key,
    betrag decimal(10, 2) not null
);

insert into konto(kontoNr, betrag) values (12858277, 10000.50), (12635277, 20000.40), (12362277, 12340.56);

select kontoNr, betrag from konto;