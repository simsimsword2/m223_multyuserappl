use m223;

drop table if exists EList;
create table EList(
	Artikelnummer int not null AUTO_INCREMENT primary key,
    Artikel varchar(45) not null,
    Preis decimal(10, 2) not null,
    Menge int,
    Beschreibung varchar(45),
    Datum datetime
);


select Artikelnummer, Artikel, Preis, Menge, Beschreibung, Datum from EList;